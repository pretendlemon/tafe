﻿using System;
using System.Windows.Forms;
// CR = Client Requirement
// FR = Functional Requirement

// AJ, J, Sprint One
// Date: 29 September 2021
// Version: 1.1
// Astronomical Processing
// Program that takes 24 values (one for each hour of the day), and lists it to the user.
// Those values are set to 0 by default, but users can add edit and remove items.
// In addition users can sort data by value.

// CR: The name of the application should be Astronomical Processing
namespace AstronomicalProcessing
{
    public partial class AstronomicalProcessing : Form
    {


        public AstronomicalProcessing()
        {
            InitializeComponent();
        }

        // CR: All data is stored as integers in an array.
        // FR: The array is of type integer
        // FR: The array has 24 elements to reflect the number of hours per day.
        // Create Variables
        private int[] astroList = new int[25];
        private int lastFreeSpace = 0;
        private int txtToInt;


        #region Functions
        // Display Function
        private void DisplayLists()
        {
            // Loop through till the value of lastFreeSpace
            // lastFreeSpace represents current array value
            ElementsList.Items.Clear();
            for (int i = 0; i < lastFreeSpace; i++)
            {
                ElementsList.Items.Add(astroList[i]);
            }
        }
        #endregion

        // CR: Data can be deleted, added and edited.
        // FR: The program must generate an error message if the text box is empty.
        // FR: The program must be able to add, edit and delete data values.
        #region Add_Edit_Remove_Buttons
        // Add button
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            // Check if textbox is not empty
            if (!string.IsNullOrEmpty(textBoxValue.Text))
            {
                if (lastFreeSpace < 24)
                {
                    // If not empty, Convert text value to int and store value to txtINt    
                    if (int.TryParse(textBoxValue.Text, out txtToInt))
                    {
                        // Add input to list
                        astroList[lastFreeSpace] = txtToInt;
                        // Add 1 to lastFreeSpace
                        lastFreeSpace++;
                        // Display value on list box
                        DisplayLists();
                        // Remove text input display on text box
                        textBoxValue.Clear();
                        StripMessage.Text = "Add Success";
                    }

                }
                // stop taking numbers when list has 24 numbers
                else
                    StripMessage.Text = "Add Failed: More than 24 Hours";
                    textBoxValue.Clear();
            }
            else
                // If textbox is empty
                StripMessage.Text = "Add Failed: Text Box is empty";
        }
        // Remove Button
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            // If list box is not empty
            if (ElementsList.SelectedIndex != -1)
            {
                // Find current value
                String selectValue = ElementsList.SelectedItem.ToString();
                int indexValue = ElementsList.FindString(selectValue);
                // Message Box
                DialogResult removeList = MessageBox.Show("Are you sure you want to delete this task?",
                    "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                // Remove if answer is yes
                if (removeList == DialogResult.Yes)
                {
                    // Remove index position and take away 1 to current astroList value
                    astroList[indexValue] = astroList[lastFreeSpace - 1];
                    // Take away 1 to lastFreeSpace
                    lastFreeSpace--;
                    StripMessage.Text = "Remove Succcess";
                    // Display value on list box
                    DisplayLists();
                    // Remove text input display on text box
                    textBoxValue.Clear();
                }
                else
                {
                    StripMessage.Text = "Remove Failed";
                }
            }
            else
                StripMessage.Text = "Remove Failed: Select a task";
        }

        // Edit button
        private void buttonEdit_Click(object sender, EventArgs e)
        {

            // If list box is not empty
            if (!string.IsNullOrEmpty(textBoxValue.Text))
            {
                // If not empty, Convert text value to int and store value to txtINt   
                if (int.TryParse(textBoxValue.Text, out txtToInt))
                {
                    // Find current item
                    String currentItem = ElementsList.SelectedItem.ToString();
                    int currentIndex = ElementsList.FindString(currentItem);
                    // Update Current item to current input
                    astroList[currentIndex] = txtToInt;
                    StripMessage.Text = "Edit Success";
                }
                else
                    StripMessage.Text = "Edit Failed";
            }
            // if list box is empty
            else
            {
                StripMessage.Text = "Edit Failed: Select a task";
            }
            DisplayLists();
            textBoxValue.Clear();
        }

        #endregion

        //FR: The array is filled with random integers to simulate the data stream (numbers between 10 and 99).
        #region SelectFunction_AutoFill_Button
        private void ElementsList_Click_1(object sender, EventArgs e)
        {
            // If list box is not empty
            if (ElementsList.SelectedIndex != -1)
            {
                // Find current selected item
                String currentItem = ElementsList.SelectedItem.ToString();
                int currentIndex = ElementsList.FindString(currentItem);
                // Display list to text box
                textBoxValue.Text = ElementsList.SelectedItem.ToString();

                // Convert text value to int and store value to txtINt   
                if (int.TryParse(textBoxValue.Text, out txtToInt))
                {
                    txtToInt = astroList[currentIndex];
                }
                else
                    StripMessage.Text = "Error Select";
            }
            else
                StripMessage.Text = "Select a task";
        }

        private void buttonFill_Click(object sender, EventArgs e)
        {
            // Create Random Class
            Random rnd = new Random();

            // loop through till the last value of astro list
            for (int i = 0; i < astroList.Length; i++)
            {
                // add 24 random numbers to list
                if (lastFreeSpace <= 24)
                {
                    astroList[i] = rnd.Next(10, 99);
                    ElementsList.Items.Add(astroList[i]);
                    lastFreeSpace = 24;
                    StripMessage.Text = "Fill Success";
                }
                else
                    StripMessage.Text = "-";


            }
            DisplayLists();
        }

        #endregion

        //FR: The sort method must be coded using the Bubble Sort algorithm.
        //FR: The search method must be coded using the Binary Search algorithm.
        //FR: The program must generate an error message if the search is not successful.
        //FR: The program must generate a message if the search is successful.
        #region Sort_Search_Buttons
        private void buttonSort_Click(object sender, EventArgs e)
        {
            // Variable
            int temp;
            // Loop through till the value of lastFreeSpace
            for (int x = 0; x < lastFreeSpace; x++)
            {
                for (int i = 0; i < lastFreeSpace; i++)
                {
                    if (astroList[i] > astroList[x])
                    {
                        // place x value to temp
                        // place i value to x
                        // place temp value to i
                        temp = astroList[x];
                        astroList[x] = astroList[i];
                        astroList[i] = temp;
                    }
                }
            }
            // Update Display
            DisplayLists();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            // Check if textbox is not empty
            if (!string.IsNullOrEmpty(textBoxValue.Text))
            {
                // If not empty, Convert text value to int and store value to searchValue    
                if (int.TryParse(textBoxValue.Text, out int searchValue))
                {
                    int min = 0;
                    int max = lastFreeSpace;
                    int mid = 0;
                    bool found = false;

                    while (min <= max)
                    {
                        mid = (min + max) / 2;
                        // Check if searchValue exist in the middle
                        if (searchValue == astroList[mid])
                        {
                            found = true;
                            break;
                        }
                        // Ignore the right half if searchValue is smaller
                        else if (searchValue < astroList[mid])
                        {
                            max = mid - 1;
                        }
                        // Ignore the left half if searchValue is greater
                        else
                            min = mid + 1;
                    }

                    if (found)
                    {
                        StripMessage.Text = "Search Success";
                        ElementsList.SelectedIndex = mid;
                        searchValue = astroList[mid];
                        textBoxValue.Clear();
                    }
                    else
                        StripMessage.Text = "Search Failed";
                }
                //  else
                // If textbox is empty
                // StripMessage.Text = "Search Failed: Text Box is empty";
            }
            else
                StripMessage.Text = "Search Failed: Text Box is empty";
        }

        #endregion


        /*
         * TESTS

            // If list box is not empty
            if (ElementsList.SelectedIndex != -1)
            {
                // Find current selected item
                String currentItem = ElementsList.SelectedItem.ToString();
                int currentIndex = ElementsList.FindString(currentItem);


                // Convert text value to int and store value to txtINt   
                if (int.TryParse(textBoxValue.Text, out txtToInt))
                {
                    txtToInt = astroList[currentIndex];
                }
                else
                    StripMessage.Text = "Error Select";
            }
            else
                // If list is empty
                StripMessage.Text = "Select a task";



            // search

        // Add input to list
                    astroList[lastFreeSpace] = txtToInt;
                    // Add 1 to lastFreeSpace
                    lastFreeSpace++;

                    // Display value on list box
                    DisplayLists();
                    // Remove text input display on text box
                    textBoxValue.Clear();
                    StripMessage.Text = "Search Success";


         */
    }
}