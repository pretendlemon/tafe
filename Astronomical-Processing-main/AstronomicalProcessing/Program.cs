﻿// Your Name, Team Name, Sprint Number
// Date:
// Version:
// Name of the program
// Brief explanation of the program and list,
// Inputs, Processes, Outputs

using System;
using System.Windows.Forms;

namespace AstronomicalProcessing
{
    static class Program
    {


        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AstronomicalProcessing());

        }
    }
}
