START
	ARRAY   SortArray = ArrayToBeSorted
	INT Temp
	
	FOR i = 0 : Count up to length of SortArray - 1

		FOR x = i + 1 : Count up to length of SortArray -1

			IF SortArray[i] > SortArray[x]
				Temp  = SortArray[i]
				SortArray[i] = SortArray[x]
				SortArray[x] = Temp
			END IF
		ENDFOR
	ENDFOR
STOP
