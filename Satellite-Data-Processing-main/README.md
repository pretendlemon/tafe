![Prototype](https://gitlab.com/pretendlemon/tafe/-/raw/main/Satellite-Data-Processing-main/Assets/Prototype.png)


# Satellite Data Processing
.Net forms app that loads sample data from a DLL into two linkedLists and displays them both in a ListView and separately in listboxes. Implements selection sort, insertion sort, binary search (iterative) and binary search (recursive) algorithms.
