# WikiList
### J

.Net Windows Forms Application that displays data structures and their attributes in a listView. Selecting an item places the item's attributes in textboxes for editing. 

![prototype](https://gitlab.com/pretendlemon/tafe/-/raw/main/WikiList-main/prototype.png)

Data structures are stored in a list with the type of "information". Each information object has a name, description, Category and can be linear or non-linear. The information class has a string array that contains the name of each category. Objects reference to the category with a byte. 
Wikilist saves/loads the wiki to a binary file. 

